<?php

namespace Innomedio\PageBundle;

use Innomedio\PageBundle\DependencyInjection\Compiler\BlockContainerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class InnomedioPageBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new BlockContainerPass());
    }
}
