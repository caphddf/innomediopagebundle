# 1.0.11

* Even more trailing slash fixes

# 1.0.10

* Fixed trailing slash bug

# 1.0.9

* Fixed bug that emptied name on pages non-roots can't change the name of

# 1.0.8

* Added tables to editor of summernote

# 1.0.7

* Created a breadcrumbs generator which automatically fills the breadscrumbs object when a page is accessed

# 1.0.6

* Fixed currentLanguage page in page form

# 1.0.5 

* Created a new helper for retrieving all data needed for a page, makes it much nicer for working in pageBlock templates
* Fixed some wrong logic, like saving the complete selectedImage path, only the file needs to be saved in the DB, the path is set in the config object of the element
* The "add to sitemap" checkbox is moved away from the root settings. Regular users should be able to edit this.
* Root users can now choose if a page can have content or not
* Root users can now define a custom controller/method in a page
* From now on, the default language is first in the page tabs
* PageBlocks weren't removed when a page was removed, this is fixed now
* Created a much better way for the dynamic routes, because of this, language codes are now being added to the slug of a page
* Fixed a bug where the page tagged as the homepage would still have a fullSlug
* You can now set a page as "manual"
* Added many root options to prevent customers from having too much freedom for certain pages
* Homepage page is now displayed correctly too
* Fixed blocks can now have a tag and some root options and can be included anywhere in the page

# 1.0.4

* Another bug fix when saving buttons

# 1.0.3

* Fixed bug where you had to define in the block if all elements have images, can be done in every seperate element now
* Fixed button amount bug

# 1.0.2

* Fixed default language in list, now uses the primary language

# 1.0.1

* Removed unique ORM constraint

# 1.0

* Start!