<?php

namespace Innomedio\PageBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Innomedio\PageBundle\Entity\PageTranslation;

class PageTranslationRepository extends EntityRepository
{
    /**
     * @param $language
     * @return PageTranslation|null
     */
    public function getHomepageByLanguage($language)
    {
        try {
            return
                $this
                    ->createQueryBuilder('t')
                    ->leftJoin('t.page', 'p')
                    ->where('p.homepage = true')
                    ->andWhere('t.language = :language')
                    ->setParameter('language', $language)
                    ->getQuery()
                    ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param $tag
     * @param $language
     * @return PageTranslation
     */
    public function getPageTranslationByTagAndLanguage($tag, $language)
    {
        try {
            return
                $this
                    ->createQueryBuilder('t')
                    ->leftJoin('t.page', 'p')
                    ->where('p.tag = :tag')
                    ->andWhere('t.language = :language')
                    ->setParameter('tag', $tag)
                    ->setParameter('language', $language)
                    ->getQuery()
                    ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * This method is used or the FrontendPageController, do not change it or use it for your logic
     *
     * @param $slug
     * @param $languageId
     * @return PageTranslation|null
     */
    public function getPageForFrontendPageController($slug, $languageCode)
    {
        /**
         * Symfony does not automatically redirect wildcard trailing slashes, so what we'll do is always check the
         * URL without a trailing slash. In the FrontendPageController we'll then do a manual redirect if there is
         * a trailing slash.
         */
        if (substr($slug, -1) === '/') {
            $slug = substr($slug, 0, -1);
        }
        try {
            $pageTranslation =
                $this
                    ->createQueryBuilder('t')
                    ->leftJoin('t.page', 'p')
                    ->where('t.slugLanguage = :languageCode')
                    ->andWhere('t.fullSlug = :slug')
                    ->andWhere('p.active = true')
                    ->andWhere('p.manual = false')
                    ->setParameter('slug', $slug)
                    ->setParameter('languageCode', $languageCode)
                    ->getQuery()
                    ->getOneOrNullResult();

            if (!$pageTranslation) {
                return new PageTranslation();
            }

            return $pageTranslation;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * This method is used or the FrontendPageController, do not change it or use it for your logic
     *
     * @param $slug
     * @param $languageId
     * @return PageTranslation|null
     */
    public function getHomepageForFrontendPageController($languageCode)
    {
        try {
            return
                $this
                    ->createQueryBuilder('t')
                    ->leftJoin('t.page', 'p')
                    ->where('t.slugLanguage = :languageCode')
                    ->andWhere('p.homepage = true')
                    ->andWhere('p.active = true')
                    ->andWhere('p.manual = false')
                    ->setParameter('languageCode', $languageCode)
                    ->getQuery()
                    ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
