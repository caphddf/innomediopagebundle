<?php

namespace Innomedio\PageBundle\Repository;

use Doctrine\ORM\NonUniqueResultException;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Innomedio\PageBundle\Entity\Page;
use Innomedio\PageBundle\Entity\PageTranslation;

class PageRepository extends NestedTreeRepository
{
    /**
     * @param $slug
     * @param $languageId
     * @return Page|null
     */
    public function getPageByFullSlug($slug, $languageId)
    {
        try {
            return
                $this
                    ->createQueryBuilder('p')
                    ->leftJoin('p.translations', 't')
                    ->where('t.language = :languageId')
                    ->andWhere('t.fullSlug = :slug')
                    ->setParameter('slug', $slug)
                    ->setParameter('languageId', $languageId)
                    ->getQuery()
                    ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
