<?php

namespace Innomedio\PageBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Innomedio\PageBundle\Entity\Page;
use Innomedio\PageBundle\Entity\PageBlockElement;

class PageBlockElementRepository extends EntityRepository
{
    /**
     * @param Page $page
     * @param $titleType
     * @return int|string
     */
    public function countTitleTypesForPage(Page $page, $titleType)
    {
        try {
            $result =
                $this
                    ->createQueryBuilder('e')
                    ->leftJoin('e.block', 'b')
                    ->select('COUNT(e.titleType) AS total')
                    ->where('b.page = :page')
                    ->andWhere('e.titleType = :titleType')
                    ->setParameter('page', $page)
                    ->setParameter('titleType', $titleType)
                    ->getQuery()
                    ->getOneOrNullResult()
            ;

            return number_format($result['total']);
        } catch (NonUniqueResultException $e) {
            return 0;
        }
    }

    /**
     * @param Page $page
     * @return array|PageBlockElement[]
     */
    public function getAllElementsOfPage(Page $page)
    {
        return
            $this
                ->createQueryBuilder('e')
                ->leftJoin('e.block', 'b')
                ->where('b.page = :page')
                ->setParameter('page', $page)
                ->getQuery()
                ->getResult()
        ;
    }
}
