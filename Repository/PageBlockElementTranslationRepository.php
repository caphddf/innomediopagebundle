<?php

namespace Innomedio\PageBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Innomedio\PageBundle\Entity\Page;
use Innomedio\PageBundle\Entity\PageBlock;
use Innomedio\PageBundle\Entity\PageBlockElementTranslation;

class PageBlockElementTranslationRepository extends EntityRepository
{
    /* @todo not finished or used yet */
    /**
     * This query is used in the controller that builds a page template. Better not use it for yourself OR SURELY don't
     * change it.
     *
     * @param PageBlock $pageBlock
     * @param $languageId
     * @return array|PageBlockElementTranslation[]
     */
    public function getElementsForPageHelperByPageBlock(PageBlock $pageBlock, $languageId)
    {
        return
            $this
                ->createQueryBuilder('t')
                ->select('t, e, b')
                ->leftJoin('t.element', 'e')
                ->leftJoin('e.block', 'b')
                ->where('t.language = :language')
                ->andWhere('e.block = :pageBlock')
                ->setParameter('language', $languageId)
                ->setParameter('pageBlock', $pageBlock)
                ->orderBy('e.sortOrder')
                ->getQuery()
                ->getResult()
        ;
    }

    /**
     * @param Page $page
     * @return array|PageBlockElementTranslation[]
     */
    public function getElementTranslationsOfPage(Page $page = null, $languageId)
    {
        $qb = $this->createQueryBuilder('t');

        return
            $qb
                ->select('t, e, b, p')
                ->leftJoin('t.element', 'e')
                ->leftJoin('e.block', 'b')
                ->leftJoin('b.parent', 'p')
                ->where('b.page = :page')
                ->andWhere('b.active = true')
                ->andWhere('t.language = :languageId')
                ->addOrderBy('b.sortOrder', 'ASC')
                ->addOrderBy('e.sortOrder', 'ASC')
                ->setParameter('page', $page)
                ->setParameter('languageId', $languageId)
                ->getQuery()
                ->getResult()
        ;
    }
}
