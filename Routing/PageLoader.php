<?php
namespace Innomedio\PageBundle\Routing;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class PageLoader extends Loader
{
    private $isLoaded = false;
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $resource
     * @param mixed $type
     * @return RouteCollection
     */
    public function load($resource, $type = null)
    {
        if (true === $this->isLoaded) {
            throw new \RuntimeException('Do not add the "page" loader twice');
        }

        $routes = new RouteCollection();

        $allPages = $this->em->getRepository('InnomedioPageBundle:PageTranslation')->findAll();
        foreach ($allPages as $pageTranslation) {
            if ($pageTranslation->getFullSlug()) {
                $path = '/{_locale}/' . $pageTranslation->getFullSlug();

                $controller = 'Innomedio\PageBundle\Controller\Frontend\PageController::display';
                $pageController = $pageTranslation->getPage()->getController();

                if ($pageController && !empty($pageController)) {
                    $controller = $pageController;
                }

                $defaults = array(
                    '_controller' => $controller,
                );

                $requirements = array(
                    '_locale' => $pageTranslation->getLanguage()->getCode()
                );

                $route = new Route($path, $defaults, $requirements);

                $routeName = 'page_route_' . $pageTranslation->getId();
                $routes->add($routeName, $route);
            }
        }

        $this->isLoaded = true;

        return $routes;
    }

    /**
     * @param mixed $resource
     * @param mixed $type
     * @return bool
     */
    public function supports($resource, $type = null)
    {
        return 'page' === $type;
    }
}