<?php
namespace Innomedio\PageBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class BlockContainerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('innomedio.page.block_container')) {
            return;
        }

        $definition = $container->findDefinition('innomedio.page.block_container');
        $taggedServices = $container->findTaggedServiceIds('innomedio.page.block');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addBlockExtension', array(new Reference($id)));
        }
    }
}