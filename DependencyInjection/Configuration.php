<?php
namespace Innomedio\PageBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('innomedio_page');

        $rootNode
            ->children()
                ->scalarNode('page_template')
                    ->isRequired()
                    ->defaultNull()
                ->end()
        ->end();

        return $treeBuilder;
    }
}
