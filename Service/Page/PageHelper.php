<?php
namespace Innomedio\PageBundle\Service\Page;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

class PageHelper
{
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param $tag
     * @param $language
     * @param boolean $fullHtml
     * @return string
     */
    public function getPageLinkByTag($tag, $language, $fullHtml = false)
    {
        $page = $this->em->getRepository('InnomedioPageBundle:PageTranslation')->getPageTranslationByTagAndLanguage($tag, $language);

        if (!$page) {
            return 'UNKNOWN-PAGE';
        }

        $url = "/" . $page->getSlugLanguage() . "/" . $page->getFullSlug();

        if ($fullHtml) {
            return '<a href="' . $url . '">' . $page->getName() . '</a>';
        }

        return $url;
    }
}