<?php
namespace Innomedio\PageBundle\Service\Page;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\BackendThemeBundle\Service\Frontend\Breadcrumbs;
use Innomedio\PageBundle\Entity\PageTranslation;

class PageBreadcrumbGenerator
{
    private $breadcrumbsCollection;
    private $em;

    /**
     * @param Breadcrumbs $breadcrumbsCollection
     */
    public function __construct(Breadcrumbs $breadcrumbsCollection, EntityManagerInterface $em)
    {
        $this->breadcrumbsCollection = $breadcrumbsCollection;
        $this->em = $em;
    }

    /**
     * @param PageTranslation $pageTranslation
     * @param bool $addHomepage
     * @return array
     */
    public function getBreadcrumbsForPage(PageTranslation $pageTranslation, $addHomepage = true)
    {
        $array = $this->recursiveFunctionForBreadcrumbs($pageTranslation);

        if ($addHomepage) {
            $homepage = $this->em->getRepository('InnomedioPageBundle:PageTranslation')->getHomepageByLanguage($pageTranslation->getLanguage()->getId());
            if ($homepage) {
                $array[$homepage->getFullSlug()] = $homepage->getName();
            }
        }

        return array_reverse($array);
    }

    /**
     * @param PageTranslation $pageTranslation
     * @param bool $addHomepage
     * @return bool
     */
    public function setBreadcrumbsObjectForPage(PageTranslation $pageTranslation, $addHomepage = true)
    {
        $array = $this->getBreadcrumbsForPage($pageTranslation, $addHomepage);

        foreach ($array as $slug => $name) {
            $this->breadcrumbsCollection->addBreadcrumb($name, $slug);
        }

        return true;
    }

    /**
     * @param PageTranslation $pageTranslation
     * @param array $array
     * @return array
     */
    private function recursiveFunctionForBreadcrumbs(PageTranslation $pageTranslation, $array = array())
    {
        $pageParent = $pageTranslation->getPage()->getParent();
        $languageId = $pageTranslation->getLanguage()->getId();

        $array[$pageTranslation->getFullSlug()] = $pageTranslation->getName();

        if ($pageParent && $pageParent->getTranslations()->get($languageId)) {
            $array = $this->recursiveFunctionForBreadcrumbs($pageParent->getTranslations()->get($languageId), $array);
        }

        return $array;
    }
}