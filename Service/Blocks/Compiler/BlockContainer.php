<?php
namespace Innomedio\PageBundle\Service\Blocks\Compiler;

use Innomedio\PageBundle\Service\Blocks\Config\Block;

class BlockContainer
{
    /**
     * @var array|Block[]
     */
    private $blocks = array();

    /**@
     * @param BlockExtensionInterface $extension
     */
    public function addBlockExtension(BlockExtensionInterface $extension)
    {
        foreach ($extension->getBlocks() as $item) {
            $this->blocks[$item->getTag()] = $item;
        }
    }

    /**
     * @return array|Block[]
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * @param $tag
     * @return Block
     */
    public function getBlock($tag)
    {
        return $this->blocks[$tag];
    }

    /**
     * @param $tag
     * @return bool
     */
    public function hasBlock($tag)
    {
        if (isset($this->blocks[$tag])) {
            return true;
        }

        return false;
    }
}