<?php
namespace Innomedio\PageBundle\Service\Blocks\Compiler;

use Innomedio\PageBundle\Service\Blocks\Config\Block;

interface BlockExtensionInterface
{
    /**
     * @return array|Block[]
     */
    public function getBlocks();
}