<?php
namespace Innomedio\PageBundle\Service\Blocks\Compiler;

use Innomedio\PageBundle\Service\Blocks\Config\Block;

class BlockExtension implements BlockExtensionInterface
{
    /**
     * @return array|Block
     */
    public function getBlocks()
    {
        return array();
    }
}