<?php
namespace Innomedio\PageBundle\Service\Blocks\JsonObjects;

class Button
{
    public $index;
    public $text;
    public $link;
}