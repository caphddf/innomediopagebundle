<?php
namespace Innomedio\PageBundle\Service\Blocks\Config;

class Block
{
    private $tag;
    private $name;
    private $template;
    private $elements = array();
    private $settings = array();
    private $expendable = false;
    private $canBeTemplate = true;

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template): void
    {
        $this->template = $template;
    }

    /**
     * @return array|Element[]
     */
    public function getElements(): array
    {
        return $this->elements;
    }

    /**
     * @param Element $element
     */
    public function addElement(Element $element)
    {
        $element->setBlock($this);
        $this->elements[$element->getTag()] = $element;
    }

    /**
     * @param $tag
     * @return Element
     */
    public function getElement($tag)
    {
        return $this->elements[$tag];
    }

    /**
     * @return bool
     */
    public function isExpendable(): bool
    {
        return $this->expendable;
    }

    /**
     * @param bool $expendable
     */
    public function setExpendable(bool $expendable): void
    {
        $this->expendable = $expendable;
    }

    /**
     * @return array|Setting[]
     */
    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * @param Setting $setting
     */
    public function addSetting(Setting $setting)
    {
        $setting->setBlock($this);
        $this->settings[$setting->getKey()] = $setting;
    }

    /**
     * @param $key
     * @return Setting
     */
    public function getSetting($key)
    {
        return $this->settings[$key];
    }

    /**
     * @return bool
     */
    public function isCanBeTemplate(): bool
    {
        return $this->canBeTemplate;
    }

    /**
     * @param bool $canBeTemplate
     */
    public function setCanBeTemplate(bool $canBeTemplate): void
    {
        $this->canBeTemplate = $canBeTemplate;
    }
}