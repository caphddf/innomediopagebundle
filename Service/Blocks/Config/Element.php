<?php
namespace Innomedio\PageBundle\Service\Blocks\Config;

class Element
{
    private $tag;
    private $name;

    /* @var $block Block */
    private $block;

    private $title = false;
    private $content = false;
    private $canChangeTitleType = true;
    private $editor = false;
    private $imageSelector = null;
    private $settings = array();
    private $buttons = 0;

    /* @var boolean */
    private $images = false;

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function hasTitle(): bool
    {
        return $this->title;
    }

    /**
     * @param bool $title
     */
    public function setTitle(bool $title): void
    {
        $this->title = $title;
    }

    /**
     * @return bool
     */
    public function hasContent(): bool
    {
        return $this->content;
    }

    /**
     * @param bool $content
     */
    public function setContent(bool $content): void
    {
        $this->content = $content;
    }

    /**
     * @return bool
     */
    public function hasEditor(): bool
    {
        return $this->editor;
    }

    /**
     * @param bool $editor
     */
    public function setEditor(bool $editor): void
    {
        $this->editor = $editor;
    }

    /**
     * @return Block
     */
    public function getBlock(): Block
    {
        return $this->block;
    }

    /**
     * @param Block $block
     */
    public function setBlock(Block $block): void
    {
        $this->block = $block;
    }

    /**
     * @return mixed
     */
    public function getImageSelector()
    {
        return $this->imageSelector;
    }

    /**
     * @param mixed $imageSelector
     */
    public function setImageSelector($imageSelector): void
    {
        $this->imageSelector = $imageSelector;
    }

    /**
     * @return int
     */
    public function getButtons(): int
    {
        return $this->buttons;
    }

    /**
     * @param int $buttons
     */
    public function setButtons(int $buttons): void
    {
        $this->buttons = $buttons;
    }

    /**
     * @return array|Setting[]
     */
    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * @return array|Setting[]
     */
    public function getMultilingualSettings()
    {
        $array = array();

        foreach ($this->getSettings() as $setting) {
            if ($setting->isMultilingual()) {
                $array[] = $setting;
            }
        }

        return $array;
    }

    /**
     * @return array|Setting[]
     */
    public function getRegularSettings()
    {
        $array = array();

        foreach ($this->getSettings() as $setting) {
            if (!$setting->isMultilingual()) {
                $array[] = $setting;
            }
        }

        return $array;
    }

    /**
     * @param Setting $setting
     */
    public function addSetting(Setting $setting)
    {
        $setting->setElement($this);
        $this->settings[$setting->getKey()] = $setting;
    }

    /**
     * @param $key
     * @return Setting
     */
    public function getSetting($key)
    {
        return $this->settings[$key];
    }

    /**
     * @return bool
     */
    public function canChangeTitleType(): bool
    {
        return $this->canChangeTitleType;
    }

    /**
     * @param bool $canChangeTitleType
     */
    public function setCanChangeTitleType(bool $canChangeTitleType): void
    {
        $this->canChangeTitleType = $canChangeTitleType;
    }

    /**
     * @return bool
     */
    public function hasImages(): bool
    {
        return $this->images;
    }

    /**
     * @param bool $images
     */
    public function setImages(bool $images): void
    {
        $this->images = $images;
    }
}