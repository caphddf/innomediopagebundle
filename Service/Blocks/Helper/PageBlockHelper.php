<?php
namespace Innomedio\PageBundle\Service\Blocks\Helper;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\PageBundle\Entity\Page as PageEntity;
use Innomedio\PageBundle\Entity\PageBlock;
use Innomedio\PageBundle\Entity\PageBlockElementTranslation;
use Innomedio\PageBundle\Service\Blocks\Compiler\BlockContainer;
use Innomedio\PageBundle\Service\Blocks\Helper\Objects\Block;
use Innomedio\PageBundle\Service\Blocks\Helper\Objects\Button;
use Innomedio\PageBundle\Service\Blocks\Helper\Objects\Element;
use Innomedio\PageBundle\Service\Blocks\Helper\Objects\Page;
use Innomedio\PageBundle\Service\Blocks\Helper\Objects\Setting;

/**
 * Creates a page object containing all elements in a certain language, so that it can easily be used in services
 * or in templates
 */
class PageBlockHelper
{
    // @todo naming of all the object should be better, too many classes with the same names (maybe change the config objects and add Config
    // @todo try to find a way to reduce queries instead of 5 per block

    private $blockContainer;
    private $em;
    private $languageId;

    /**
     * @param BlockContainer $blockContainer
     * @param EntityManagerInterface $em
     */
    public function __construct(BlockContainer $blockContainer, EntityManagerInterface $em)
    {
        $this->blockContainer = $blockContainer;
        $this->em = $em;
    }

    /**
     * @param $languageId
     */
    public function setLanguageId($languageId)
    {
        $this->languageId = $languageId;
    }

    /**
     * @param PageEntity $pageEntity
     * @return Page
     */
    public function getPage(PageEntity $pageEntity)
    {
        $page = new Page();
        $page->setBlocks($this->getAllBlocksOfPage($pageEntity));

        return $page;
    }

    /**
     * @param PageEntity $pageEntity
     * @return array|Block[]
     */
    public function getAllBlocksOfPage(PageEntity $pageEntity)
    {
        $blocks = array();

        foreach ($pageEntity->getPageBlocks() as $pageBlock) {
            if ($pageBlock->isActive()) {
                $contentPageBlock = $this->getCorrectPageBlock($pageBlock);

                $block = new Block();
                $block->setTemplate($this->blockContainer->getBlock($contentPageBlock->getTag())->getTemplate());
                $block->setElements($this->getAllElementsOfPageBlock($contentPageBlock));
                $block->setSettings($this->getSettingsOfPageBlock($pageBlock));

                $blocks[] = $block;
            }
        }

        return $blocks;
    }

    /**
     * @param $tag
     * @return Block|null
     */
    public function getPageBlockByTag($tag)
    {
        $pageBlock = $this->em->getRepository('InnomedioPageBundle:PageBlock')->findOneBy(array('customTag' => $tag));

        // @todo duplicate code with the method above

        if ($pageBlock) {
            $contentPageBlock = $this->getCorrectPageBlock($pageBlock);

            $block = new Block();
            $block->setTemplate($this->blockContainer->getBlock($contentPageBlock->getTag())->getTemplate());
            $block->setElements($this->getAllElementsOfPageBlock($contentPageBlock));
            $block->setSettings($this->getSettingsOfPageBlock($pageBlock));

            return $block;
        }

        return null;
    }

    /**
     * @param PageBlock $pageBlock
     * @return array|Setting[]
     */
    public function getSettingsOfPageBlock(PageBlock $pageBlock)
    {
        $settings = array();

        $allSettings = json_decode($pageBlock->getSettings());

        if (is_object($allSettings)) {
            foreach ($allSettings as $key => $value) {
                $setting = new Setting();
                $setting->setKey($key);
                $setting->setValue($value);

                $settings[$key] = $setting;
            }
        }

        return $settings;
    }

    /**
     * @param PageBlock $pageBlock
     * @return array|Element[]
     */
    public function getAllElementsOfPageBlock(PageBlock $pageBlock)
    {
        $elements = array();

        foreach ($pageBlock->getElements() as $element) {
            $selectedImageDir = $this->blockContainer->getBlock($pageBlock->getTag())->getElement($element->getTag())->getImageSelector();

            $newElement = new Element();
            $newElement->setId($element->getId());
            $newElement->setTitleType($element->getTitleType());
            $newElement->setSelectedImage($selectedImageDir . $element->getSelectedImage());
            $newElement->setTag($element->getTag());

            $elementTranslation = $element->getTranslations()->get($this->languageId);

            if ($elementTranslation) {
                $newElement->setTitle($elementTranslation->getTitle());
                $newElement->setContent($elementTranslation->getContent());
                $newElement->setSettings($this->getAllSettingsOfElement($elementTranslation));
                $newElement->setButtons($this->getButtonsOfElement($elementTranslation));
            }

            $elements[$element->getTag()] = $newElement;
        }

        return $elements;
    }

    /**
     * @param PageBlockElementTranslation $elementTranslation
     * @return array|Setting[]
     */
    public function getAllSettingsOfElement(PageBlockElementTranslation $elementTranslation)
    {
        // @todo settings if it's a fixed block (so the parent block)

        $settings = array();

        $translationSettings = array();
        $elementSettings = array();

        $jsonTranslationSettings = json_decode($elementTranslation->getSettings());
        $jsonElementTranslations = json_decode($elementTranslation->getElement()->getSettings());

        if (is_object($jsonTranslationSettings)) {
            $translationSettings = $jsonTranslationSettings;
        }

        if (is_object($jsonElementTranslations)) {
            $elementSettings = $jsonElementTranslations;
        }

        $allSettings = array_merge(
            (array) $translationSettings,
            (array) $elementSettings
        );

        foreach ($allSettings as $key => $value) {
            $setting = new Setting();
            $setting->setKey($key);
            $setting->setValue($value);

            $settings[$key] = $setting;
        }

        return $settings;
    }

    /**
     * @param PageBlockElementTranslation $elementTranslation
     * @return array|Button[]
     */
    public function getButtonsOfElement(PageBlockElementTranslation $elementTranslation)
    {
        $buttons = array();

        $obj = json_decode($elementTranslation->getButtonJson());

        if ($obj) {
            /* @var $button \Innomedio\PageBundle\Service\Blocks\JsonObjects\Button */
            foreach ($obj as $key => $button) {
                $newButton = new Button();
                $newButton->setIndex($button->index);
                $newButton->setLink($button->link);
                $newButton->setText($button->text);

                $buttons[$button->index] = $newButton;
            }
        }

        return $buttons;
    }

    /**
     * If a pageBlock has a parent, which is the part that contains all the content (since it's a fixed block), return
     * the parent, otherwise, return the original block
     *
     * @param PageBlock $pageBlock
     * @return PageBlock|null
     */
    private function getCorrectPageBlock(PageBlock $pageBlock)
    {
        if ($pageBlock->getParent()) {
            $pageBlock = $pageBlock->getParent();
        }

        return $pageBlock;
    }
}