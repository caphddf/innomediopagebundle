<?php
namespace Innomedio\PageBundle\Service\Blocks\Helper\Objects;

class Element
{
    private $tag;
    private $id;
    private $title;
    private $content;
    private $titleType;
    private $selectedImage;

    /* @var array|Setting[] */
    private $settings = array();

    /* @var array|Button[] */
    private $buttons;

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return array|Setting[]
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param array|Setting[] $settings
     */
    public function setSettings($settings): void
    {
        $this->settings = $settings;
    }

    /**
     * @param Setting $setting
     */
    public function addSetting(Setting $setting)
    {
        $this->settings[$setting->getKey()] = $setting;
    }

    /**
     * @param $key
     * @return Setting
     */
    function getSetting($key)
    {
        if (isset($this->settings[$key])) {
            return $this->settings[$key];
        }

        return new Setting();
    }

    /**
     * @return array|Button[]
     */
    public function getButtons()
    {
        return $this->buttons;
    }

    /**
     * @param array|Button[] $buttons
     */
    public function setButtons($buttons): void
    {
        $this->buttons = $buttons;
    }

    /**
     * @param Button $button
     */
    public function addButton(Button $button)
    {
        $this->buttons[$button->getIndex()] = $button;
    }

    /**
     * @param $index
     * @return bool|Button
     */
    function getButton($index)
    {
        if (isset($this->buttons[$index])) {
            return $this->buttons[$index];
        }

        return new Button;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitleType()
    {
        return $this->titleType;
    }

    /**
     * @param mixed $titleType
     */
    public function setTitleType($titleType): void
    {
        $this->titleType = $titleType;
    }

    /**
     * @return mixed
     */
    public function getSelectedImage()
    {
        return $this->selectedImage;
    }

    /**
     * @param mixed $selectedImage
     */
    public function setSelectedImage($selectedImage): void
    {
        $this->selectedImage = $selectedImage;
    }
}