<?php
namespace Innomedio\PageBundle\Service\Blocks\Helper\Objects;

class Page
{
    /* @var array|Block[] */
    private $blocks;

    /**
     * @return array|Block[]
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * @param array|Block[] $blocks
     */
    public function setBlocks($blocks): void
    {
        $this->blocks = $blocks;
    }

    /**
     * @param Block $block
     */
    public function addBlock(Block $block)
    {
        $this->blocks[] = $block;
    }
}