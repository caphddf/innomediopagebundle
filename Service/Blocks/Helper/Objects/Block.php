<?php
namespace Innomedio\PageBundle\Service\Blocks\Helper\Objects;

class Block
{
    private $tag;
    private $template;

    /* @var array|Setting[] */
    private $settings = array();

    /* @var array|Element[] */
    private $elements = array();

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return array|Setting[]
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param array|Setting[] $settings
     */
    public function setSettings($settings): void
    {
        $this->settings = $settings;
    }

    /**
     * @param Setting $setting
     */
    public function addSetting(Setting $setting)
    {
        $this->settings[$setting->getKey()] = $setting;
    }

    /**
     * @param $key
     * @return Setting
     */
    function getSetting($key)
    {
        if (isset($this->settings[$key])) {
            return $this->settings[$key];
        }

        return new Setting();
    }

    /**
     * @return array|Element[]
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param array|Element[] $elements
     */
    public function setElements($elements): void
    {
        $this->elements = $elements;
    }

    /**
     * @param Element $element
     */
    public function addElement(Element $element)
    {
        $this->elements[$element->getTag()] = $element;
    }

    /**
     * @param $tag
     * @return bool|Element
     */
    public function getElement($tag)
    {
        if (isset($this->elements[$tag])) {
            return $this->elements[$tag];
        }

        return new Element();
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template): void
    {
        $this->template = $template;
    }
}