<?php
namespace Innomedio\PageBundle\Service\Backend;

use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarExtension;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarItem;
use Symfony\Component\Routing\Router;

class PageSidebarExtension extends SidebarExtension
{
    private $router;
    private $mainItem;

    /**
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @return array|SidebarItem
     */
    public function getSidebars()
    {
        $return = array(
            $this->mainItem(),
            $this->fixedBlocks(),
            $this->pages()
        );

        return $return;
    }

    /**
     * @return SidebarItem
     */
    public function mainItem()
    {
        $main = new SidebarItem();
        $main->setIcon('fa-file-o');
        $main->setName('innomedio.page.pages');
        $main->setTag('pages');
        $main->setRole('ROLE_PAGES');
        $main->setLink($this->router->generate('innomedio.pages'));
        $main->setOrder(0);

        $this->mainItem = $main;

        return $main;
    }

    /**
     * @return SidebarItem
     */
    public function pages()
    {
        $pages = new SidebarItem();
        $pages->setIcon('fa-file-o');
        $pages->setName('innomedio.page.page_management');
        $pages->setLink($this->router->generate('innomedio.pages.list'));
        $pages->setTag('pages_list');
        $pages->setParent($this->mainItem());
        $pages->setOrder(10);
        $pages->setRole('ROLE_PAGES');

        return $pages;
    }

    /**
     * @return SidebarItem
     */
    public function fixedBlocks()
    {
        $pages = new SidebarItem();
        $pages->setIcon('fa-bars');
        $pages->setName('innomedio.page.fixed_blocks');
        $pages->setLink($this->router->generate('innomedio.pages.fixed_blocks'));
        $pages->setTag('pages_fixed_blocks');
        $pages->setParent($this->mainItem());
        $pages->setOrder(20);
        $pages->setRole('ROLE_PAGES');

        return $pages;
    }
}