<?php
namespace Innomedio\PageBundle\Service\Backend;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\PageBundle\Entity\Page;
use Symfony\Component\Filesystem\Filesystem;

class PageUrlBuilder
{
    private $em;

    /**
     * PageUrlBuilder constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     *
     */
    public function buildPageUrls()
    {
        $pages = $this->em->getRepository('InnomedioPageBundle:Page')->findBy(array(), array('lvl' => 'desc'));
        foreach ($pages as $page)
        {
            foreach ($page->getTranslations() as $pageTranslation) {
                if ($page->isHomepage()) {
                    $url = '';
                } else {
                    $url = $pageTranslation->getUrl();
                }

                $url = $this->addParentUrl($page, $pageTranslation->getLanguage()->getId(), $url);
                $pageTranslation->setFullSlug($url);

                $this->em->persist($pageTranslation);
                $this->em->flush();
            }
        }
    }

    /**
     * @param Page $page
     * @param $languageId
     * @param string $url
     */
    public function addParentUrl(Page $page, $languageId, $url = '')
    {
        if ($page->getParent()) {
            $parentUrl = $page->getParent()->getTranslations()->get($languageId)->getUrl();
            if (!$parentUrl || empty($parentUrl) || $page->getParent()->isHomepage()) {
                $parentUrl = '';
            } else {
                $parentUrl = $parentUrl . "/";
            }

            $url = $parentUrl . $url;
            $url = $this->addParentUrl($page->getParent(), $languageId, $url);
        } else {
            /*
            Leave it for now, but the language is now stored in the table to make routing work with the _locale parameter

            $language = $this->em->getRepository('InnomedioBackendThemeBundle:Language')->find($languageId);
            $url = "/" . $language->getCode() . "/" . $url;
            */
        }

        return $url;
    }
}