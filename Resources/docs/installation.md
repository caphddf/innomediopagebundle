# Installation

**Make sure the InnomedioBackendThemeBundle is installed correctly.**

Add the bundle to your composer file: 

```
"require": {
    "innomedio/page-bundle": "^1.0"
},

"repositories": [
    {
        "type": "git",
        "url": "git@gitlab.com:innomedio/internal/symfony/InnomedioPageBundle.git"
    }
]
```

Do ```composer update```.

Add the routing file to your **config/routes/annotations.yaml** file:

```
innomedio_page:
    resource: '@InnomedioPageBundle/Resources/config/routes.yaml'
```

Generate all tables:

```
php bin/console doctrine:migrations:diff
php bin/console doctrine:migrations:migrate
```

Make sure this role is available: **ROLE_PAGES**