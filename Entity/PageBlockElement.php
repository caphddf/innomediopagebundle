<?php
namespace Innomedio\PageBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\Loggable
 * @ORM\Entity(repositoryClass="Innomedio\PageBundle\Repository\PageBlockElementRepository")
 */
class PageBlockElement
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", nullable=true)
     */
    private $tag;

    /**
     * @var null|int
     * @ORM\Column(type="integer", nullable=true)
     */
    private $titleType = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $selectedImage = null;

    /**
     * @var PageBlock
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\PageBundle\Entity\PageBlock", inversedBy="elements")
     * @ORM\JoinColumn(name="block_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $block;

    /**
     * @ORM\Column(type="integer")
     */
    private $sortOrder = 0;

    /**
     * @var ArrayCollection|PageBlockElementTranslation[]
     * @ORM\OneToMany(targetEntity="Innomedio\PageBundle\Entity\PageBlockElementTranslation", mappedBy="element", cascade={"persist", "remove"}, indexBy="language_id")
     */
    private $translations;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $settings;

    /**
     * PageBlockElement constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return PageBlock
     */
    public function getBlock(): PageBlock
    {
        return $this->block;
    }

    /**
     * @param PageBlock $block
     */
    public function setBlock(PageBlock $block): void
    {
        $this->block = $block;
    }

    /**
     * @return mixed
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param mixed $sortOrder
     */
    public function setSortOrder($sortOrder): void
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @param PageBlockElementTranslation $translation
     */
    public function addTranslation(PageBlockElementTranslation $translation)
    {
        $translation->setElement($this);

        if (!$this->translations->contains($translation)) {
            $this->translations->add($translation);
        }
    }

    /**
     * @param PageBlockElementTranslation $translation
     */
    public function removeTranslation(PageBlockElementTranslation $translation)
    {
        $this->translations->remove($translation);
    }

    /**
     * @return ArrayCollection|PageBlockElementTranslation[]
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param ArrayCollection|PageBlockElementTranslation[] $translations
     */
    public function setTranslations($translations): void
    {
        $this->translations = $translations;
    }

    /**
     * @return null|int
     */
    public function getTitleType(): ?int
    {
        return $this->titleType;
    }

    /**
     * @param null|int $titleType
     */
    public function setTitleType(?int $titleType): void
    {
        $this->titleType = $titleType;
    }

    /**
     * @return mixed
     */
    public function getSelectedImage()
    {
        return $this->selectedImage;
    }

    /**
     * @param mixed $selectedImage
     */
    public function setSelectedImage($selectedImage): void
    {
        $this->selectedImage = $selectedImage;
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param mixed $settings
     */
    public function setSettings($settings): void
    {
        $this->settings = $settings;
    }
}