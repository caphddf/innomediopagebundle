<?php
namespace Innomedio\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Innomedio\BackendThemeBundle\Entity\Language;

/**
 * @Gedmo\Loggable
 * @ORM\Table(name="page_translation", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="page_language_translation", columns={"language_id", "page_id"})
 * })
 * @ORM\Entity(repositoryClass="Innomedio\PageBundle\Repository\PageTranslationRepository")
 */
class PageTranslation
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Language|null
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\BackendThemeBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $language;

    /**
     * @var Page
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\PageBundle\Entity\Page", inversedBy="translations")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $page;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metaDescription;

    /**
     * We have two seperate slug fields, this one should not be used within the website, and is only needed to generate
     * the full slug below. After a page has been added/edited, a slug is being generated based on the parents of the page.
     *
     * @Gedmo\Versioned
     * @Gedmo\Slug(fields={"name"}, unique=false)
     * @ORM\Column(type="string", nullable=true)
     */
    private $url;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", nullable=true)
     */
    private $fullSlug;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", nullable=true)
     */
    private $slugLanguage;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return Language|null
     */
    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    /**
     * @param Language|null $language
     */
    public function setLanguage(?Language $language): void
    {
        $this->language = $language;
    }

    /**
     * @return Page
     */
    public function getPage(): Page
    {
        return $this->page;
    }

    /**
     * @param Page $page
     */
    public function setPage(Page $page): void
    {
        $this->page = $page;
    }

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = $url;
    }

    /**
     * Needed for the TreeSlugHandler
     * @return mixed|null
     */
    public function getParentTranslation()
    {
        return $this->getPage()->getTranslations()->get($this->getLanguage()->getId());
    }

    /**
     * @return mixed
     */
    public function getFullSlug()
    {
        return $this->fullSlug;
    }

    /**
     * @param mixed $fullSlug
     */
    public function setFullSlug($fullSlug): void
    {
        $this->fullSlug = $fullSlug;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSlugLanguage()
    {
        return $this->slugLanguage;
    }

    /**
     * @param mixed $slugLanguage
     */
    public function setSlugLanguage($slugLanguage): void
    {
        $this->slugLanguage = $slugLanguage;
    }
}