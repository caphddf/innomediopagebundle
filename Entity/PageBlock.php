<?php
namespace Innomedio\PageBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\Loggable
 * @ORM\Entity(repositoryClass="Innomedio\PageBundle\Repository\PageBlockRepository")
 */
class PageBlock
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $tag;

    /**
     * @var null|PageBlock
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\PageBundle\Entity\PageBlock")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $parent;

    /**
     * @var Page
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\PageBundle\Entity\Page", inversedBy="pageBlocks")
     * @ORM\JoinColumn(name="page_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $page;

    /**
     * @var $elements ArrayCollection|PageBlockElement[]
     * @ORM\OneToMany(targetEntity="Innomedio\PageBundle\Entity\PageBlockElement", mappedBy="block", cascade={"persist", "remove"}, indexBy="tag")
     */
    private $elements;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $active = false;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $sortOrder = 999;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $settings;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $customName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $customTag;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $blockedForPage = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return null|Page
     */
    public function getPage(): ?Page
    {
        return $this->page;
    }

    /**
     * @param Page $page
     */
    public function setPage(Page $page): void
    {
        $this->page = $page;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getSortOrder(): int
    {
        return $this->sortOrder;
    }

    /**
     * @param int $sortOrder
     */
    public function setSortOrder(int $sortOrder): void
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return ArrayCollection|PageBlockElement[]
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param mixed $settings
     */
    public function setSettings($settings): void
    {
        $this->settings = $settings;
    }

    /**
     * @return PageBlock|null
     */
    public function getParent(): ?PageBlock
    {
        return $this->parent;
    }

    /**
     * @param PageBlock|null $parent
     */
    public function setParent(?PageBlock $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getCustomName()
    {
        return $this->customName;
    }

    /**
     * @param mixed $customName
     */
    public function setCustomName($customName): void
    {
        $this->customName = $customName;
    }

    /**
     * @return mixed
     */
    public function getCustomTag()
    {
        return $this->customTag;
    }

    /**
     * @param mixed $customTag
     */
    public function setCustomTag($customTag): void
    {
        $this->customTag = $customTag;
    }

    /**
     * @return bool
     */
    public function isBlockedForPage(): bool
    {
        return $this->blockedForPage;
    }

    /**
     * @param bool $blockedForPage
     */
    public function setBlockedForPage(bool $blockedForPage): void
    {
        $this->blockedForPage = $blockedForPage;
    }
}