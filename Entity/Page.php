<?php
namespace Innomedio\PageBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\Tree(type="nested")
 * @Gedmo\Loggable
 * @ORM\Entity(repositoryClass="Innomedio\PageBundle\Repository\PageRepository")
 */
class Page
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", nullable=true)
     */
    private $tag;

    /**
     * @var boolean
     * @Gedmo\Versioned
     * @ORM\Column(type="boolean")
     */
    private $active = false;

    /**
     * @var boolean
     * @Gedmo\Versioned
     * @ORM\Column(type="boolean")
     */
    private $inSitemap = false;

    /**
     * @ORM\Column(type="integer")
     */
    private $sortOrder = 999;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $homepage = false;

    /**
     * @var ArrayCollection|PageBlock[]
     * @ORM\OneToMany(targetEntity="Innomedio\PageBundle\Entity\PageBlock", mappedBy="page", cascade={"persist", "remove"})
     * @ORM\OrderBy({"sortOrder" =  "ASC"})
     */
    private $pageBlocks;

    /**
     * @var ArrayCollection|PageTranslation[]
     * @ORM\OneToMany(targetEntity="Innomedio\PageBundle\Entity\PageTranslation", mappedBy="page", cascade={"persist", "remove"}, indexBy="language_id")
     */
    private $translations;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $hasContent = true;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", nullable=true)
     */
    private $controller;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $manual = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $blockName = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $blockActive = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $blockSubpages = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $blockSitemap = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $disableMeta = false;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @var Page
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="Innomedio\PageBundle\Entity\Page")
     * @ORM\JoinColumn(name="tree_root", referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @var Page
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="Innomedio\PageBundle\Entity\Page", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var ArrayCollection|Page[]
     * @ORM\OneToMany(targetEntity="Innomedio\PageBundle\Entity\Page", mappedBy="parent")
     * @ORM\OrderBy({"sortOrder" = "ASC"})
     */
    private $children;

    /**
     * Page constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->pageBlocks = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function isInSitemap(): bool
    {
        return $this->inSitemap;
    }

    /**
     * @param bool $inSitemap
     */
    public function setInSitemap(bool $inSitemap): void
    {
        $this->inSitemap = $inSitemap;
    }

    /**
     * @param PageTranslation $translation
     */
    public function addTranslation(PageTranslation $translation)
    {
        $translation->setPage($this);

        if (!$this->translations->contains($translation)) {
            $this->translations->add($translation);
        }
    }

    /**
     * @param PageTranslation $translation
     */
    public function removeTranslation(PageTranslation $translation)
    {
        $this->translations->remove($translation);
    }

    /**
     * @return ArrayCollection|PageTranslation[]
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param ArrayCollection|PageTranslation[] $translations
     */
    public function setTranslations($translations): void
    {
        $this->translations = $translations;
    }

    /**
     * @return Page
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param Page $parent
     */
    public function setParent(Page $parent = null): void
    {
        $this->parent = $parent;
    }

    /**
     * @return ArrayCollection|Page[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param ArrayCollection|Page[] $children
     */
    public function setChildren($children): void
    {
        $this->children = $children;
    }

    /**
     * @return mixed
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * @param mixed $sortOrder
     */
    public function setSortOrder($sortOrder): void
    {
        $this->sortOrder = $sortOrder;
    }

    /**
     * @return bool
     */
    public function isHomepage(): bool
    {
        return $this->homepage;
    }

    /**
     * @param bool $homepage
     */
    public function setHomepage(bool $homepage): void
    {
        $this->homepage = $homepage;
    }

    /**
     * @return ArrayCollection|PageBlock[]
     */
    public function getPageBlocks()
    {
        return $this->pageBlocks;
    }

    /**
     * @return bool
     */
    public function hasContent(): bool
    {
        return $this->hasContent;
    }

    /**
     * @param bool $hasContent
     */
    public function setHasContent(bool $hasContent): void
    {
        $this->hasContent = $hasContent;
    }

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param mixed $controller
     */
    public function setController($controller): void
    {
        $this->controller = $controller;
    }

    /**
     * @return bool
     */
    public function isManual(): bool
    {
        return $this->manual;
    }

    /**
     * @param bool $manual
     */
    public function setManual(bool $manual): void
    {
        $this->manual = $manual;
    }

    /**
     * @return bool
     */
    public function isBlockName(): bool
    {
        return $this->blockName;
    }

    /**
     * @param bool $blockName
     */
    public function setBlockName(bool $blockName): void
    {
        $this->blockName = $blockName;
    }

    /**
     * @return bool
     */
    public function isBlockSubpages(): bool
    {
        return $this->blockSubpages;
    }

    /**
     * @param bool $blockSubpages
     */
    public function setBlockSubpages(bool $blockSubpages): void
    {
        $this->blockSubpages = $blockSubpages;
    }

    /**
     * @return bool
     */
    public function isBlockActive(): bool
    {
        return $this->blockActive;
    }

    /**
     * @param bool $blockActive
     */
    public function setBlockActive(bool $blockActive): void
    {
        $this->blockActive = $blockActive;
    }

    /**
     * @return bool
     */
    public function isBlockSitemap(): bool
    {
        return $this->blockSitemap;
    }

    /**
     * @param bool $blockSitemap
     */
    public function setBlockSitemap(bool $blockSitemap): void
    {
        $this->blockSitemap = $blockSitemap;
    }

    /**
     * @return bool
     */
    public function isDisableMeta(): bool
    {
        return $this->disableMeta;
    }

    /**
     * @param bool $disableMeta
     */
    public function setDisableMeta(bool $disableMeta): void
    {
        $this->disableMeta = $disableMeta;
    }
}