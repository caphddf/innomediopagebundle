<?php
namespace Innomedio\PageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Innomedio\BackendThemeBundle\Entity\Language;

/**
 * @Gedmo\Loggable
 * @ORM\Table(name="page_block_element_translation", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="page_block_element_language_translation", columns={"language_id", "element_id"})
 * })
 * @ORM\Entity(repositoryClass="Innomedio\PageBundle\Repository\PageBlockElementTranslationRepository")
 */
class PageBlockElementTranslation
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Language|null
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\BackendThemeBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $language;

    /**
     * @var PageBlockElement
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\PageBundle\Entity\PageBlockElement", inversedBy="translations")
     * @ORM\JoinColumn(name="element_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $element;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    private $buttonJson;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $settings;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return Language|null
     */
    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    /**
     * @param Language|null $language
     */
    public function setLanguage(?Language $language): void
    {
        $this->language = $language;
    }

    /**
     * @return PageBlockElement
     */
    public function getElement(): PageBlockElement
    {
        return $this->element;
    }

    /**
     * @param PageBlockElement $element
     */
    public function setElement(PageBlockElement $element): void
    {
        $this->element = $element;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getButtonJson()
    {
        return $this->buttonJson;
    }

    /**
     * @param mixed $buttonJson
     */
    public function setButtonJson($buttonJson): void
    {
        $this->buttonJson = $buttonJson;
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param mixed $settings
     */
    public function setSettings($settings): void
    {
        $this->settings = $settings;
    }
}