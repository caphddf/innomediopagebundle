<?php
namespace Innomedio\PageBundle\Twig;

use Innomedio\BackendThemeBundle\Service\Language\CurrentLanguage;
use Innomedio\PageBundle\Service\Blocks\Helper\Objects\Block;
use Innomedio\PageBundle\Service\Blocks\Helper\PageBlockHelper;
use Innomedio\PageBundle\Service\Page\PageHelper;
use Twig\TwigFunction;

class PageExtension extends \Twig_Extension
{
    private $pageHelper;
    private $currentLanguage;
    private $pageBlockHelper;

    /**
     * @param PageHelper $pageHelper
     * @param CurrentLanguage $currentLanguage
     */
    public function __construct(PageHelper $pageHelper, CurrentLanguage $currentLanguage, PageBlockHelper $pageBlockHelper)
    {
        $this->currentLanguage = $currentLanguage;
        $this->pageHelper = $pageHelper;
        $this->pageBlockHelper = $pageBlockHelper;
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('getBlock', array($this, 'getBlock')),
            new TwigFunction('pageLink', array($this, 'getPageLink'), [
                'is_safe' => ['html']
            ])
        );
    }

    /**
     * @param $tag
     * @param $languageId
     * @param boolean $fullHtml
     * @return string
     */
    public function getPageLink($tag, $fullHtml = false, $languageId = null)
    {
        if (!$languageId) {
            $languageId = $this->currentLanguage->getLanguageId();
        }

        return $this->pageHelper->getPageLinkByTag($tag, $languageId, $fullHtml);
    }

    /**
     * @param $tag
     * @return Block|null
     */
    public function getBlock($tag)
    {
        $this->pageBlockHelper->setLanguageId($this->currentLanguage->getLanguageId());
        return $this->pageBlockHelper->getPageBlockByTag($tag);
    }
}