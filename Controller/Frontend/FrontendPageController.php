<?php
namespace Innomedio\PageBundle\Controller\Frontend;

use Innomedio\PageBundle\Entity\PageTranslation;
use Innomedio\PageBundle\Service\Blocks\Helper\Objects\Page;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * This controller catches all uri that cannot be linked to any hard-coded routes in other controllers/methods. It then
 * checks if there's a page that can be linked to the url.
 *
 * Because hard-coded routes with a trailing slash will end up here too, we made it so that the Repository method return
 * an empty object instead of null. Because if it would return null, an exception would be thrown and we would not be able
 * to redirect the hard-coded routes that have a trailing slash.
 */
class FrontendPageController extends Controller
{
    // @todo remove deprecated repository_method

    /**
     * @Route("/{_locale}/{full_slug}",
     *     name="innomedio.pages.frontend_page_method",
     *     requirements={"_locale" : "^[a-z]{2}$", "full_slug"=".+"},
     * )
     * @ParamConverter("pageTranslation", options={
     *     "mapping": {"full_slug": "slug", "_locale": "languageCode"},
     *     "repository_method": "getPageForFrontendPageController",
     *     "map_method_signature": true
     * })
     * @param PageTranslation $pageTranslation
     * @param Request $request
     * @return Response
     */
    public function display(PageTranslation $pageTranslation, Request $request)
    {
        $currentUrl = $request->getUri();
        if (substr($currentUrl, -1) === '/') {
            return $this->redirect(substr($currentUrl, 0, -1), 301);
        }

        if (!$pageTranslation->getId()) {
            throw new NotFoundHttpException();
        }

        return $this->handlePage($pageTranslation);
    }

    /**
     * @Route("/{_locale}",
     *     name="innomedio.pages.frontend_homepage_method",
     *     requirements={"_locale" : "^[a-z]{2}$"},
     * )
     * @ParamConverter("pageTranslation", options={
     *     "mapping": {"_locale": "languageCode"},
     *     "repository_method": "getHomepageForFrontendPageController",
     *     "map_method_signature": true
     * })
     * @return Response
     */
    public function displayHomepage(PageTranslation $pageTranslation)
    {
        return $this->handlePage($pageTranslation);
    }

    /**
     * @param PageTranslation $pageTranslation
     * @return Response
     */
    private function handlePage(PageTranslation $pageTranslation)
    {
        $this->get('innomedio.backend_theme.frontend.meta')->setTitle(
            $pageTranslation->getMetaTitle()
        );

        $this->get('innomedio.backend_theme.frontend.meta')->setDescription(
            $pageTranslation->getMetaDescription()
        );

        $currentLanguageId = $pageTranslation->getLanguage()->getId();

        $helper = $this->get('innomedio.page.page_block_helper');
        $helper->setLanguageId($currentLanguageId);

        $page = $pageTranslation->getPage();

        if ($page->getController()) {
            return $this->forward($page->getController());
        }

        $pageObject = $helper->getPage($page);

        $this->get('innomedio.page.breadcrumbs_generator')->setBreadcrumbsObjectForPage($pageTranslation);

        return $this->render($this->getParameter('innomedio_page.page_template'), array(
            'page' => $pageObject
        ));
    }
}