<?php
namespace Innomedio\PageBundle\Controller\Backend\Page;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\BackendThemeBundle\Service\Ajax\AjaxResponse;
use Innomedio\PageBundle\Entity\PageBlock;
use Innomedio\PageBundle\Form\FixedPageBlockType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("has_role('ROLE_ROOT')")
 * @Route("/pages")
 */
class FixedBlockSettingsController extends BackendThemeController
{
    /**
     * @Route(
     *     "/fixed-block-settings/{id}",
     *     name="innomedio.pages.fixed_blocks.settings.popup",
     *     requirements={
     *         "id" = "\d+"
     *     }
     * )
     * @return Response
     */
    public function showPopup(PageBlock $pageBlock)
    {
        $form = $this->createForm(FixedPageBlockType::class, $pageBlock);

        return $this->render('@InnomedioPage/backend/pages/fixed_blocks/popup_settings.html.twig', array(
            'form' => $form->createView(),
            'pageBlock' => $pageBlock
        ));
    }

    /**
     * @Route(
     *     "/fixed-block-settings/{id}/submit",
     *     name="innomedio.pages.fixed_blocks.settings.popup.submit",
     *     requirements={
     *         "id" = "\d+"
     *     }
     * )
     * @param PageBlock $pageBlock
     * @param Request $request
     * @return JsonResponse
     */
    public function submit(PageBlock $pageBlock, Request $request)
    {
        $response = new AjaxResponse();
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(FixedPageBlockType::class, $pageBlock);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($pageBlock);
            $em->flush();

            $response->setSuccess(true);
        } else {
            $response->setErrors($this->get('innomedio.backend_theme.error_parser')->getErrors($form, $pageBlock));
        }

        return new JsonResponse($response->getResponse());
    }
}