<?php
namespace Innomedio\PageBundle\Controller\Backend\Page;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\PageBundle\Entity\PageBlock;
use Innomedio\PageBundle\Entity\PageBlockElement;
use Innomedio\PageBundle\Service\Blocks\Config\Block;
use Innomedio\PageBundle\Service\Blocks\Config\Element;
use Innomedio\PageBundle\Service\Blocks\Config\Setting;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("has_role('ROLE_PAGES')")
 * @Route("/pages")
 */
class PageBlockSettingsController extends BackendThemeController
{
    /**
     * @Route(
     *     "/settings/{id}",
     *     name="innomedio.pages.settings.popup",
     *     requirements={
     *         "id" = "\d+"
     *     }
     * )
     * @throws \Exception
     * @return Response
     */
    public function showSettingPopup(PageBlock $pageBlock)
    {
        $config = $this->getCorrectConfigSettings($pageBlock);

        $pageBlockSettings = array();
        $json = json_decode($pageBlock->getSettings());

        if ($json) {
            foreach ($json as $key => $value) {
                $pageBlockSettings[$key] = $value;
            }
        }

        return $this->render('@InnomedioPage/backend/pages/settings/popup_pageblock.html.twig', array(
            'configSettings' => $config,
            'pageBlockSettings' => $pageBlockSettings,
            'pageBlock' => $pageBlock
        ));
    }

    /**
     * @Route(
     *     "/settings/submit/{id}",
     *     name="innomedio.pages.settings.popup.submit",
     *     requirements={
     *         "id" = "\d+"
     *     }
     * )
     * @param $type
     * @param $id
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function editSettings(PageBlock $pageBlock, Request $request)
    {
        $json = array();

        $config = $this->getCorrectConfigSettings($pageBlock);

        /* @var $data Setting */
        foreach ($config->getSettings() as $key => $data) {
            if (isset($request->request->get('config')[$key])) {
                $value = $request->request->get('config')[$key];

                if ($request->request->get('config')[$key] === 'on') {
                    $value = true;
                }

                $json[$key] = $value;
            } else {
                $json[$key] = null;
            }
        }

        $pageBlock->setSettings(json_encode($json));

        $em = $this->getDoctrine()->getManager();
        $em->persist($pageBlock);
        $em->flush();

        return new JsonResponse(array('success' => true));
    }

    /**
     * @param PageBlock $pageBlock
     * @return Block
     */
    private function getCorrectConfigSettings(PageBlock $pageBlock)
    {
        if ($pageBlock->getParent()) {
            $config = $this->get('innomedio.page.block_container')->getBlock($pageBlock->getParent()->getTag());
        } else {
            $config = $this->get('innomedio.page.block_container')->getBlock($pageBlock->getTag());
        }

        return $config;
    }
}