<?php
namespace Innomedio\PageBundle\Controller\Backend\Page;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\BackendThemeBundle\Service\Ajax\AjaxResponse;
use Innomedio\PageBundle\Entity\Page;
use Innomedio\PageBundle\Entity\PageBlock;
use Innomedio\PageBundle\Form\PageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("has_role('ROLE_PAGES')")
 * @Route("/pages")
 */
class PageContentController extends BackendThemeController
{

    /**
     * @Route("/list", name="innomedio.pages.list")
     * @return Response
     */
    public function list()
    {
        $this->header()->addBreadcrumb('innomedio.page.pages', $this->generateUrl('innomedio.pages'));
        $this->header()->addBreadcrumb('innomedio.page.page_management');

        return $this->render('@InnomedioPage/backend/pages/list.html.twig', array(
            'pages' => $this->getDoctrine()->getRepository('InnomedioPageBundle:Page')->findBy(array('parent' => null), array('sortOrder' => 'asc'))
        ));
    }

    /**
     * @Route(
     *     "/form/{id}",
     *     name="innomedio.pages.form",
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     * )
     *
     * @param Page|null $page
     * @return Response
     */
    public function form(Page $page = null)
    {
        $action = 'edit';

        if (!$page) {
            $page = new Page();
            $action = 'add';
        }

        $page = $this->get('innomedio.backend_theme.object_language_helper')->updateLanguages($page);

        $this->header()->addBreadcrumb('innomedio.page.pages', $this->generateUrl('innomedio.pages'));
        $this->header()->addBreadcrumb('innomedio.page.page_management', $this->generateUrl('innomedio.pages.list'));
        $this->header()->addBreadcrumb('innomedio.page.page_management.' . $action);

        $form = $this->createForm(PageType::class, $page, array(
            'languageId' => $this->get('innomedio.backend_theme.current_language')->getDefaultLanguage(),
            'em' => $this->getDoctrine()->getManager()
        ));
        $form->add('submit', SubmitType::class, array(
            'label' => 'innomedio.backend_theme.global.' . $action
        ));

        return $this->render('@InnomedioPage/backend/pages/form.html.twig', array(
            'form' => $form->createView(),
            'page' => $page,
            'activeNav' => 'pages',
            'activeSubnav' => 'pages_list',
            'languages' => $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->findBy(array(), array('main' => 'desc'))
        ));
    }

    /**
     * @Route(
     *     "/submit/{id}",
     *     name="innomedio.pages.submit",
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     * )
     *
     * @param Page|null $page
     * @param Request $request
     * @return JsonResponse
     */
    public function submit(Page $page = null, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $response = new AjaxResponse();

        if (!$page) {
            $page = new Page();
        }

        $form = $this->createForm(PageType::class, $page, array(
            'languageId' => $this->get('innomedio.backend_theme.current_language')->getDefaultLanguage(),
            'em' => $this->getDoctrine()->getManager()
        ));
        $form->handleRequest($request);

        $homepageCheck = $em->getRepository('InnomedioPageBundle:Page')->findOneBy(array('homepage' => true));

        if ($form->isSubmitted() && $form->isValid()) {
            if ($page->isHomepage() === true && $homepageCheck && $homepageCheck->getId() !== $page->getId()) {
                $response->setMessage($this->get('translator')->trans('innomedio.page.already_has_homepage'));
            } else {
                $em->persist($page);
                $em->flush();

                $this->get('innomedio.page.url_builder')->buildPageUrls();

                $response->setSuccess(true);
                $response->setRedirect($this->generateUrl('innomedio.pages.list'));
            }
        } else {
            $response->setErrors($this->get('innomedio.backend_theme.error_parser')->getErrors($form, $page));
        }

        return new JsonResponse($response->getResponse());
    }

    /**
     * @Route(
     *     "/remove/{id}",
     *     name="innomedio.pages.remove",
     *     requirements={
     *         "id" = "\d+"
     *     }
     * )
     *
     * @param Page $page
     * @return JsonResponse
     */
    public function remove(Page $page)
    {
        $response = new AjaxResponse();

        if (!$page->getChildren() || $page->getChildren()->count() > 0) {
            $response->setMessage($this->get('translator')->trans('innomedio.page.cant_remove_has_children'));
        } elseif ($page->getTag()) {
            $response->setMessage($this->get('translator')->trans('innomedio.page.cant_remove_tagged_page'));
        } else {
            $elements = $this->getDoctrine()->getRepository('InnomedioPageBundle:PageBlockElement')->getAllElementsOfPage($page);
            foreach ($elements as $element) {
                $this->get('innomedio.backend_theme.upload.file_removal')->removeAllFilesForItem('element', $element->getId());
            }

            $em = $this->getDoctrine()->getManager();
            $em->remove($page);
            $em->flush();

            $response->setSuccess(true);
        }

        return $response->getJsonResponse();
    }

    /**
     * @Route("/order", name="innomedio.pages.change_order")
     * @param Request $request
     * @return JsonResponse
     */
    public function changeOrder(Request $request)
    {
        $response = new AjaxResponse();

        $em = $this->getDoctrine()->getManager();

        $pages = $request->request->get('pages');
        foreach ($pages as $key => $pageId) {
            $page = $em->getRepository('InnomedioPageBundle:Page')->find($pageId);

            if ($page) {
                $page->setSortOrder($key);
            }

            $em->persist($page);
        }

        $em->flush();

        $response->setSuccess(true);
        return $response->getJsonResponse();
    }

    /**
     * @Route(
     *     "/content/{id}",
     *     name="innomedio.pages.content",
     *     requirements={
     *         "id" = "\d+"
     *     }
     * )
     *
     * @param Page $page
     * @return Response
     */
    public function content(Page $page)
    {
        $this->header()->addBreadcrumb('innomedio.page.pages', $this->generateUrl('innomedio.pages'));
        $this->header()->addBreadcrumb('innomedio.page.page_management', $this->generateUrl('innomedio.pages.list'));
        $this->header()->addBreadcrumb('innomedio.page.page_management.edit', $this->generateUrl('innomedio.pages.form', array('id' => $page->getId())));
        $this->header()->addBreadcrumb('innomedio.page.edit_content', null);

        return $this->render('@InnomedioPage/backend/pages/content.html.twig', array(
            'activeNav' => 'pages',
            'activeSubnav' => 'pages_list',
            'h1Amount' => $this->getDoctrine()->getRepository('InnomedioPageBundle:PageBlockElement')->countTitleTypesForPage($page, 1),
            'page' => $page,
            'blocks' => $this->get('innomedio.page.block_container')->getBlocks(),
            'pageBlocks' =>
                $this
                    ->getDoctrine()
                    ->getRepository('InnomedioPageBundle:PageBlock')
                    ->findBy(array('page' => $page), array('sortOrder' => 'asc')),
            'customBlocks' =>
                $this
                    ->getDoctrine()
                    ->getRepository('InnomedioPageBundle:PageBlock')
                    ->findBy(array('page' => null, 'customTag' => null), array('customName' => 'asc'))
        ));
    }

    /**
     * @Route(
     *     "/ajax-add-block/{id}",
     *     name="innomedio.pages.content.add_block",
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     * )
     *
     * @param Page|null $page
     * @param Request $request
     * @return JsonResponse
     */
    public function addBlockToPage(Page $page = null, Request $request)
    {
        $tag = $request->request->get('tag');

        if (is_numeric($tag) || $this->get('innomedio.page.block_container')->hasBlock($tag)) {
            $pageBlock = new PageBlock();

            if ($page) {
                $pageBlock->setPage($page);
            } else {
                $pageBlock->setCustomName($request->request->get('name'));
            }

            /* If the tag is numeric, it means a fixed block is selected */
            if (is_numeric($tag)) {
                $parentBlock = $this->getDoctrine()->getRepository('InnomedioPageBundle:PageBlock')->find($tag);
                $pageBlock->setParent($parentBlock);
                $pageBlock->setActive(true);
            } else {
                $pageBlock->setTag($tag);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($pageBlock);
            $em->flush();
        }

        $sort = 'sortOrder';

        if (!$page) {
            $sort = 'customName';
        }

        $blocks = $this->getDoctrine()->getRepository('InnomedioPageBundle:PageBlock')->findBy(array('page' => $page), array($sort => 'asc'));

        $templateVars = array(
            'page' => $page,
            'pageBlocks' => $blocks,
            'blocks' => $this->get('innomedio.page.block_container')->getBlocks()
        );

        if ($page) {
            $templateVars['h1Amount'] =
                $this
                    ->getDoctrine()
                    ->getRepository('InnomedioPageBundle:PageBlockElement')
                    ->countTitleTypesForPage($page, 1);
        }

        return new JsonResponse(array(
            'success' => true,
            'html' => $this->renderView('@InnomedioPage/backend/pages/_content_blocks.html.twig', $templateVars)
        ));
    }

    /**
     * @Route(
     *     "/ajax-set-block-active",
     *     name="innomedio.pages.content.block_active",
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function setBlockActive(Request $request)
    {
        $block = $this->getDoctrine()->getRepository('InnomedioPageBundle:PageBlock')->find($request->request->get('id'));

        if ($block) {
            $active = false;

            if ($request->request->get('active') === 'true') {
                $active = true;
            }

            $block->setActive($active);

            $em = $this->getDoctrine()->getManager();
            $em->persist($block);
            $em->flush();
        }

        return new JsonResponse();
    }

    /**
     * @Route(
     *     "/ajax-remove-block/{id}",
     *     name="innomedio.pages.content.remove_block",
     *     requirements={
     *         "id" = "\d+"
     *     }
     * )
     *
     * @param PageBlock $block
     * @return JsonResponse
     */
    public function removeBlockFromPage(PageBlock $block)
    {
        $em = $this->getDoctrine()->getManager();

        foreach ($block->getElements() as $element) {
            $this->get('innomedio.backend_theme.upload.file_removal')->removeAllFilesForItem('element', $element->getId());
        }

        $em->remove($block);
        $em->flush();

        return new JsonResponse(array(
            'success' => true
        ));
    }

    /**
     * @Route("/ajax-block-order", name="innomedio.pages.content.block_order")
     * @param Request $request
     * @return JsonResponse
     */
    public function changeBlockOrder(Request $request)
    {
        $response = new AjaxResponse();

        $em = $this->getDoctrine()->getManager();

        $blocks = $request->request->get('blocks');
        foreach ($blocks as $key => $blockId) {
            $block = $em->getRepository('InnomedioPageBundle:PageBlock')->find($blockId);

            if ($block) {
                $block->setSortOrder($key);
            }

            $em->persist($block);
        }

        $em->flush();

        $response->setSuccess(true);
        return $response->getJsonResponse();
    }

    /**
     * @Route("/fixed-blocks", name="innomedio.pages.fixed_blocks")
     * @return Response
     */
    public function fixedBlocks()
    {
        $this->header()->addBreadcrumb('innomedio.page.pages', $this->generateUrl('innomedio.pages'));
        $this->header()->addBreadcrumb('innomedio.page.fixed_blocks', $this->generateUrl('innomedio.pages.fixed_blocks'));

        return $this->render('@InnomedioPage/backend/pages/content.html.twig', array(
            'activeNav' => 'pages',
            'activeSubnav' => 'pages_fixed_blocks',
            'page' => null,
            'blocks' => $this->get('innomedio.page.block_container')->getBlocks(),
            'pageBlocks' =>
                $this
                    ->getDoctrine()
                    ->getRepository('InnomedioPageBundle:PageBlock')
                    ->findBy(
                        array('page' => null),
                        array('customName' => 'asc')
                    )
        ));
    }

}