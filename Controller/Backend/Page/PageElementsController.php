<?php
namespace Innomedio\PageBundle\Controller\Backend\Page;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\PageBundle\Entity\PageBlock;
use Innomedio\PageBundle\Entity\PageBlockElement;
use Innomedio\PageBundle\Entity\PageBlockElementTranslation;
use Innomedio\PageBundle\Service\Blocks\Config\Setting;
use Innomedio\PageBundle\Service\Blocks\JsonObjects\Button;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Innomedio\PageBundle\Service\Blocks\Helper\Objects\Element as HelperElement;

/**
 * @Security("has_role('ROLE_PAGES')")
 * @Route("/pages/element")
 */
class PageElementsController extends BackendThemeController
{
    /**
     * @Route(
     *     "/form/{id}/{elementTag}",
     *     name="innomedio.pages.element.form",
     *     requirements={
     *         "id" = "\d+"
     *     }
     * )
     *
     * @param PageBlock $block
     * @param $elementTag
     * @return Response
     */
    public function form(PageBlock $block, $elementTag)
    {
        /* @var $elementContent array|HelperElement[] */
        $elementContent = array();
        $languages = $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->findAll();
        foreach ($languages as $language) {
            $helper = $this->get('innomedio.page.page_block_helper');
            $helper->setLanguageId($language->getId());

            if ($helper->getAllElementsOfPageBlock($block) && isset($helper->getAllElementsOfPageBlock($block)[$elementTag])) {
                $elementContent[$language->getId()] = $helper->getAllElementsOfPageBlock($block)[$elementTag];
            }
        }

        return $this->render('@InnomedioPage/backend/pages/_pageblock_popup.html.twig', array(
            'finder' => new Finder(),
            'pageBlock' => $block,
            'projectDir' => $this->getParameter('kernel.project_dir'),
            'elementConfig' => $this->get('innomedio.page.block_container')->getBlock($block->getTag())->getElement($elementTag),
            'elementContent' => $elementContent,
            'languages' => $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->findBy(array(), array('main' => 'desc'))
        ));
    }

    /**
     * @Route(
     *     "/submit/{id}/{elementTag}",
     *     name="innomedio.pages.element.submit",
     *     requirements={
     *         "id" = "\d+"
     *     }
     * )
     *
     * @param PageBlock $block
     * @param $elementTag
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function submitPageBlock(PageBlock $block, $elementTag, Request $request)
    {
        $response = array();

        $languages = $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->findAll();
        $element = $this->getDoctrine()->getRepository('InnomedioPageBundle:PageBlockElement')->findOneBy(array('block' => $block, 'tag' => $elementTag));

        $configBlock = $this->get('innomedio.page.block_container')->getBlock($block->getTag());

        if (!$element) {
            $element = new PageBlockElement();
            $element->setBlock($block);
            $element->setTag($elementTag);
        }

        if ($request->request->get('titleType')) {
            $element->setTitleType($request->request->get('titleType'));
        } else {
            $element->setTitleType(null);
        }

        if ($request->request->get('selectImage')) {
            $element->setSelectedImage($request->request->get('selectImage'));
        }

        // If the block is fixed (no custom elements by user), get the sortOrder of the element
        $sortOrder = 0;
        foreach ($configBlock->getElements() as $configElement) {
            if ($configElement->getTag() !== $elementTag) {
                $sortOrder++;
            } else {
                break;
            }
        }

        // @todo don't change sortorder for expendable elements
        $element->setSortOrder($sortOrder);

        // Create the json with all general settings
        $validSettingsForElement = $configBlock->getElement($elementTag)->getRegularSettings();
        $jsonSettings = json_encode($this->loopThroughSettingsAndCreateArray($validSettingsForElement, $request->request->get('config')));
        $element->setSettings($jsonSettings);

        // Loop through all languages stuff
        foreach ($languages as $language) {
            $buttonsArray = array();
            $languageConfigArray = array();

            $elementLanguage =
                $this
                    ->getDoctrine()
                    ->getRepository('InnomedioPageBundle:PageBlockElementTranslation')
                    ->findOneBy(array('element' => $element, 'language' => $language));

            if (!$elementLanguage) {
                $elementLanguage = new PageBlockElementTranslation();
                $elementLanguage->setLanguage($language);
            }

            $fields = $request->request->get('content')[$language->getCode()];
            if (isset($fields['title'])) {
                $elementLanguage->setTitle($fields['title']);
            }

            if (isset($fields['content'])) {
                $elementLanguage->setContent($fields['content']);
            }

            if ($configBlock->getElement($elementTag)->getButtons() > 0) {
                for ($i = 0; $i <= ($configBlock->getElement($elementTag)->getButtons() - 1); $i++) {
                    $buttonObject = new Button();
                    $buttonObject->language = $language->getCode();
                    $buttonObject->index = $i;
                    $buttonObject->text = $request->request->get('button')[$language->getCode()][$i]['text'];
                    $buttonObject->link = $request->request->get('button')[$language->getCode()][$i]['link'];

                    $buttonsArray[$i] = $buttonObject;
                }
            }

            $elementLanguage->setButtonJson(json_encode($buttonsArray));
            $element->addTranslation($elementLanguage);

            $validSettingsForElement = $configBlock->getElement($elementTag)->getMultilingualSettings();

            $jsonSettings = json_encode(
                $this->loopThroughSettingsAndCreateArray(
                    $validSettingsForElement,
                    $request->request->get('config')[$language->getCode()]
                )
            );

            $elementLanguage->setSettings($jsonSettings);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($element);
        $em->flush();

        $response['success'] = true;

        if ($block->getPage()) {
            $response['h1Amount'] = $this->getDoctrine()->getRepository('InnomedioPageBundle:PageBlockElement')->countTitleTypesForPage($block->getPage(), 1);
        }

        return new JsonResponse($response);
    }

    /**
     * @Route(
     *     "/images/{id}/{elementTag}",
     *     name="innomedio.pages.element.images",
     *     requirements={
     *         "id" = "\d+"
     *     }
     * )
     * @param PageBlock $block
     * @param $elementTag
     * @return Response
     */
    public function elementImages(PageBlock $block, $elementTag)
    {
        if (!$this->getParameter('innomedio_news.news_has_images')) {
            throw new NotFoundHttpException();
        }

        $element = $this->getDoctrine()->getRepository('InnomedioPageBundle:PageBlockElement')->findOneBy(array('block' => $block, 'tag' => $elementTag));

        if (!$element) {
            $element = new PageBlockElement();
            $element->setBlock($block);
            $element->setTag($elementTag);

            $em = $this->getDoctrine()->getManager();
            $em->persist($element);
            $em->flush();
        }

        $this->header()->setHidden(true);

        return $this->render('@InnomedioBackendTheme/image/list.html.twig', array(
            'standalone' => true,
            'tag' => 'element',
            'id' => $element->getId(),
            'images' =>
                $this
                    ->getDoctrine()
                    ->getRepository('InnomedioBackendThemeBundle:Image')
                    ->findBy(array('tagId' => $element->getId(), 'tag' => 'element'), array('sortOrder' => 'asc'))
        ));
    }

    /**
     * @param array|Setting[] $validSettings
     * @param $postArray
     * @return array
     */
    private function loopThroughSettingsAndCreateArray($validSettings, $postArray)
    {
        $returnArray = array();

        foreach ($validSettings as $setting) {
            $key = $setting->getKey();

            if (isset($postArray[$key])) {
                $value = $postArray[$key];

                if ($postArray[$key] === 'on') {
                    $value = true;
                }

                $returnArray[$key] = $value;
            } else {
                $returnArray[$key] = null;
            }
        }

        return $returnArray;
    }
}