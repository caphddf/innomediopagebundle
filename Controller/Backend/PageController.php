<?php
namespace Innomedio\PageBundle\Controller\Backend;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends BackendThemeController
{
    /**
     * @Route("/pages", name="innomedio.pages")
     */
    public function index()
    {
        $this->header()->addBreadcrumb('innomedio.page.pages');

        return $this->render('@InnomedioBackendTheme/item_page.html.twig', array(
            'type' => 'pages'
        ));
    }
}
