<?php
namespace Innomedio\PageBundle\Form;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\BackendThemeBundle\Form\Type\ThemedCheckboxType;
use Innomedio\PageBundle\Entity\Page;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType
{
    private $languageId;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->languageId = $options['languageId'];
        $currentPageId = null;

        if ($builder->getData()) {
            $currentPageId = $builder->getData()->getId();
        }

        $selectOptions = $this->createOptions($options['em']->getRepository('InnomedioPageBundle:Page')->findBy(array('parent' => null, 'homepage' => false), array('sortOrder' => 'asc')), 0, $currentPageId);

        $builder
            ->add('tag', TextType::class, array(
                'required' => false,
                'label' => 'innomedio.backend_theme.global.tag'
            ))
            ->add('active', ThemedCheckboxType::class, array(
                'label' => 'innomedio.page.page_active'
            ))
            ->add('inSitemap', ThemedCheckboxType::class, array(
                'label' => 'innomedio.page.in_sitemap'
            ))
            ->add('hasContent', ThemedCheckboxType::class, array(
                'label' => 'innomedio.page.has_content'
            ))
            ->add('controller', TextType::class, array(
                'label' => 'innomedio.page.controller',
                'required' => false
            ))
            ->add('homepage', ThemedCheckboxType::class, array(
                'label' => 'innomedio.page.is_homepage'
            ))
            ->add('manual', ThemedCheckboxType::class, array(
                'label' => 'innomedio.page.is_manual'
            ))
            ->add('blockName', ThemedCheckboxType::class, array(
                'label' => 'innomedio.page.block_name'
            ))
            ->add('blockActive', ThemedCheckboxType::class, array(
                'label' => 'innomedio.page.block_active'
            ))
            ->add('blockSubpages', ThemedCheckboxType::class, array(
                'label' => 'innomedio.page.block_subpages'
            ))
            ->add('blockSitemap', ThemedCheckboxType::class, array(
                'label' => 'innomedio.page.block_sitemap'
            ))
            ->add('disableMeta', ThemedCheckboxType::class, array(
                'label' => 'innomedio.page.disable_meta'
            ))
            ->add('translations', CollectionType::class, array(
                'allow_extra_fields' => true,
                'entry_type' => PageTranslationType::class,
                'by_reference' => false,
                'allow_add' => true
            ))
            ->add('parentId', ChoiceType::class, array(
                'mapped' => false,
                'multiple' => false,
                'required' => false,
                'expanded' => false,
                'choice_translation_domain' => false,
                'attr' => array('data-provide' => 'selectpicker', 'data-live-search' => 'true'),
                'label' => 'innomedio.page.parent',
                'choices' => $selectOptions
            ))
        ;

        /* @var $em EntityManagerInterface */
        $em = $options['em'];

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($em) {
            /* @var $page Page */
            $page = $event->getData();
            $form = $event->getForm();

            if ($form->get('parentId')->getData()) {
                $parentId = $form->get('parentId')->getData();
                $page->setParent($em->getReference('InnomedioPageBundle:Page', $parentId));
            }
        });

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            /* @var $page Page */
            $page = $event->getData();
            $form = $event->getForm();

            if ($page->getParent()) {
                $form->get('parentId')->setData($page->getParent()->getId());
            }
        });

        $builder->get('parentId')->resetViewTransformers();

    }

    /**
     * @param $pages
     * @param $counter
     * @param array $collection
     * @return array
     */
    public function createOptions($pages, $counter, $currentPageId = null, $collection = array())
    {
        $counter++;

        /* @var $page Page */
        foreach ($pages as $page) {
            if ($page->getId() !== $currentPageId) {
                $text = '';

                if ($counter > 1) {
                    for ($x = 0; $x <= ($counter - 1) * 3; $x++) {
                        $text .= "-";
                    }
                }

                $text .= " " . $page->getTranslations()->get($this->languageId)->getName();

                $collection[$text] = $page->getId();
                $collection = array_merge($collection, $this->createOptions($page->getChildren(), $counter, $currentPageId, $collection));
            }
        }

        return $collection;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Page::class,
            'allow_extra_fields' => true,
            'languageId' => null,
            'em' => null
        ));
    }
}