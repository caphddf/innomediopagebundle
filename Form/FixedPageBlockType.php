<?php
namespace Innomedio\PageBundle\Form;

use Innomedio\BackendThemeBundle\Form\Type\SwitchType;
use Innomedio\BackendThemeBundle\Form\Type\ThemedCheckboxType;
use Innomedio\PageBundle\Entity\PageBlock;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FixedPageBlockType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('customName', TextType::class, array(
                'required' => true,
                'label' => 'innomedio.backend_theme.global.name'
            ))
            ->add('customTag', TextType::class, array(
                'required' => false,
                'label' => 'innomedio.backend_theme.global.tag'
            ))
            ->add('blockedForPage', SwitchType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PageBlock::class,
            'allow_extra_fields' => true
        ));
    }
}