<?php
namespace Innomedio\PageBundle\Form;

use Innomedio\BackendThemeBundle\Entity\Language;
use Innomedio\PageBundle\Entity\PageTranslation;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageTranslationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', EntityType::class, array(
                'class' => Language::class,
                'choice_label' => 'name',
                'label' => 'innomedio.backend_theme.label.code'
            ))
            ->add('name', TextType::class, array(
                'label' => 'innomedio.page.page_name',
                'required' => false
            ))
            ->add('metaTitle', TextareaType::class, array(
                'label' => 'innomedio.backend_theme.global.meta_title',
                'required' => false,
                'attr' => array('class' => 'meta_field meta_title', 'rows' => 3)
            ))
            ->add('metaDescription', TextareaType::class, array(
                'label' => 'innomedio.backend_theme.global.meta_description',
                'required' => false,
                'attr' => array('class' => 'meta_field meta_description', 'rows' => 4)
            ))
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /* @var $page PageTranslation */
            $page = $event->getData();
            $page->setSlugLanguage($page->getLanguage()->getCode());
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PageTranslation::class,
            'allow_extra_fields' => true
        ));
    }
}